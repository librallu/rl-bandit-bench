use rl_bandit::bandit::{Action, Reward};
use rand_distr::{Normal, Distribution};
use rand::thread_rng;

/**
 * Implements a bandit benchmark. Each Action has an average between 0 and 1 and a variance of 1.
 */
pub struct BanditBenchmark {
    /// average value of each arm
    arms: Vec<Reward>,
    /// cached value of the best arm of the benchmark
    best_arm: usize
}

impl BanditBenchmark {

    /**
     * - **n:** nb arms 
     */
    pub fn new(n: Action) -> Self {
        let mut arms = Vec::new();
        let normal = Normal::new(0.0, 1.0).unwrap();
        let mut best_arm = 0;
        for i in 0..n {
            arms.push(normal.sample(&mut thread_rng()));
            if arms[best_arm] < arms[i] {
                best_arm = i;
            }
        }
        Self { arms: arms, best_arm: best_arm }
    }

    /**
     * simulates the reward produced by an action
     */
    pub fn simulate(&self, action: Action) -> Reward {
        let normal = Normal::new(self.arms[action], 1.0).unwrap();
        return normal.sample(&mut thread_rng());
    }

    /**
     * gets the best arm
     */
    pub fn get_best_arm(&self) -> usize { self.best_arm }
}