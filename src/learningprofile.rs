use std::fs::File;
use std::io::{Write};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::string::String;


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LearningPoint {
    pub avg_reward: f64,
    pub opt_ratio: f64
}

pub fn export_learning_data(v:&Vec<LearningPoint>, filename:String, title:String) {
    let mut file = match File::create(filename.as_str()) {
        Err(why) => panic!("couldn't create {}: {}", filename, why),
        Ok(file) => file
    };
    match file.write(serde_json::to_string(&json!({
        "title": title,
        "points": v
    })).unwrap().as_bytes()) {
        Err(why) => panic!("couldn't write: {}",why),
        Ok(_) => {}
    };
}