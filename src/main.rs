mod benchmark;
mod learningprofile;

use benchmark::BanditBenchmark;
use learningprofile::{LearningPoint, export_learning_data};

use rl_bandit::bandit::{Bandit, UpdateType};
use rl_bandit::bandits::egreedy::EGreedy;
use rl_bandit::bandits::ucb::UCB;
use rl_bandit::bandits::stochastic_gradient::StochasticGradient;

/**
 * runs a multi-armed bandit benchmark and automatically exports data to a file.
 * **parameters:**
 * n: nb trials
 * m: horizon (time steps)
 * nbarms: nb bandit arms
 * function returning a bandit algorithm
 * 
 */
fn run_benchmark<B:Bandit>(n:usize, m:usize, nbarms:usize, bandit_factory: impl Fn() -> B) {
    println!("running benchmark with: {}", bandit_factory().str());
    let mut t_reward = vec![0.; n];  // average reward over time
    let mut t_opt = vec![0.; n];  // nb optimal decisions found so far
    for _ in 0..m {
        // create benchmark (10 possible actions in the example)
        let bench = BanditBenchmark::new(nbarms);
        // create an epsilon bandit algorithm
        let mut bandit = bandit_factory();
        for i in 0..n {
            let action = bandit.choose();
            if action == bench.get_best_arm() {
                t_opt[i] += 1.;
            }
            let reward = bench.simulate(action);
            bandit.update(action, reward);
            t_reward[i] += reward;
        }
    }
    let mut export_data = [].to_vec();
    for i in 0..n {
        export_data.push( LearningPoint {
            avg_reward: t_reward[i]/(m as f64),
            opt_ratio: t_opt[i]/(m as f64)
        });
    }
    export_learning_data(
        &export_data,
        "out/".to_string()
            + &bandit_factory().str().replace(" ", "_").replace(".","").replace("=","")
            + &".json".to_string(),
        bandit_factory().str()
    );
}

fn main() {
    run_benchmark(1000, 2000, 10, || EGreedy::new(10, 0.0, 0.0, UpdateType::Average)); // greedy
    run_benchmark(1000, 2000, 10, || EGreedy::new(10, 0.01, 0.0, UpdateType::Average)); // egreedy 0.01
    run_benchmark(1000, 2000, 10, || EGreedy::new(10, 0.1, 0.0, UpdateType::Average)); // egreedy 0.1
    run_benchmark(1000, 2000, 10, || EGreedy::new(10, 0.1, 0.0, UpdateType::Nonstationary(0.1))); // egreedy 0.1
    run_benchmark(1000, 2000, 10, || EGreedy::new(10, 0.0, 5.0, UpdateType::Average));  // optimistic greedy
    run_benchmark(1000, 2000, 10, || UCB::new(10, 1.)); // UCB
    run_benchmark(1000, 2000, 10, || StochasticGradient::new(10, 0.1, false)); // Stochastic gradient (without baseline)
    run_benchmark(1000, 2000, 10, || StochasticGradient::new(10, 0.1, true)); // Stochastic gradient (with baseline)
    run_benchmark(1000, 2000, 10, || StochasticGradient::new(10, 0.4, false)); // Stochastic gradient (without baseline)
    run_benchmark(1000, 2000, 10, || StochasticGradient::new(10, 0.4, true)); // Stochastic gradient (with baseline)
}
