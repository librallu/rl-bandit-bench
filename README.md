# rl-bandit-bench

Implements a k-armed bandit testbed. Useful to quickly evaluate some bandits algorithms. It requires the [rl-bandit](https://gitlab.com/librallu/rl-bandit) crate. The testbed is inspired by ``Reinforcement Learning: An Introduction'' by *Richard S. Sutton* and *Andrew G. Barto*.

![](imgs/e_greedy.png)
![](imgs/comp1.png)

## structure

- **printAvg.py** takes as parameters some output json files. Display the average reward over time
- **printOpt.py** takes as parameters some output json files. Display the average percentage of optimal actions selected.
- **src/**
    - **benchmark.rs** implements the k-arm bandit testbed
    - **learningprofile.rs** data structure to encode the learning profile and how to export it to json
    - **main.rs** executes the benchmark on various bandit algorithms and prints results within the **out/** directory.