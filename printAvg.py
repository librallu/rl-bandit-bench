#!/usr/bin/python3

import sys
import json
import matplotlib.pyplot as plt

def main(args):
    titles = []
    for filename in args[1:]:
        with open(filename, 'r') as f:
            data = json.load(f)
            title = data['title']
            titles.append(title)
            x = list(range(len(data['points'])))
            y = [ e['avg_reward'] for e in data['points'] ]
            plt.plot(x, y)
            plt.ylabel('avg reward')
            plt.xlabel('time')
    # plt.xscale("log")
    # plt.yscale("log")
    plt.legend(titles)
    plt.show()



if __name__ == "__main__":
    main(sys.argv)